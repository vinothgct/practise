// Pointer
// • Pointer is address variable
// • It can store the address of data
// • Pointer are used for accessing heap memory
// • 5 Arithmetic operations are allowed pointer
// • p++ - move pointer to next element
// • p - - move pointer to previous element
// • p+k gives address of kth element form pointer location to right
// • p-k gives address of kth element from pointer location to left
// • q-p gives number of elements between 2 pointers p and q
// • Pointers can be of many levels
// • Double pointer is used for accessing 2D arrays






#include <iostream>
using namespace std;
int main()
{
    int A[5] = {2, 4, 6, 8, 10};
    int *p = A,*q; 
    q= &A[4];

    cout<<*q<<endl;
    cout << *p << endl;//op = 2
    cout << endl;
    p++;
    cout << *p << endl;//op = 4
    cout << endl;
    p--;
    cout << *p << endl;//op = 2
    cout << endl;
    cout << p << endl;//op = address of p
    cout << p + 2 << endl;//op = address of p+2
    cout << endl;
    cout << *p << endl;//op = value of p ie,p=2
    cout << *(p + 2) << endl;//op = value of p+2 ie, p=6
    cout << endl;
    cout << q - p << endl; //op = 4
    cout << p - q << endl;//op = -4

}