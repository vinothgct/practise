//pointer practice questions
int f(int x, int *py, int **ppz)
{
int y, z;
**ppz += 1;//4+1 = 5
z = **ppz;
*py += 2;//5+2 = 7
y = *py;
x += 3;//4+3 = 7
return x + y + z; // 5+7+7 = 19
}
void main()
{
int c, *b, **a;
c = 4;
b = &c;
a = &b;
printf("%d ", f(c, b, a));
return 0;
}