#include <stdio.h>
int main(void)
{
int i = 10;
const int *ptr= &i; //compilation error.it is read only integer pointer pointing to integer i.because ptr initialized by const keyword 
*ptr = 100;
printf("i = %d\n", i);
return 0;
}