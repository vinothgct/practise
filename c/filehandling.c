//find size of file 
#include <stdio.h>
#include <stdlib.h>
int main()
{
    FILE *fp;
    int len;
    fp = fopen("file.txt","r");
    if(fp==NULL)
    {
        printf("Error opening file");
    }
    fseek(fp,0,SEEK_END);
    len=ftell(fp);
    fclose(fp);
    printf("total size of file txt = %d bytes \n",len);
}

//output : number of element in  file 