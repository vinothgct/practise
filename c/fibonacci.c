//Write to program to create fibonacci series for given number
#include <stdio.h>
void main()
{
    int first,second,sum,counter,num;
    counter=0;
    printf("enter the terms ");
    scanf("%d",&num);
    printf("enter first and second number ");
    scanf("%d%d",&first,&second);
    //printf("The fibonacci series numbers are &d\t&d\t",first,second);
    while(counter<num)
    {
        sum=first+second;
        printf("%d\n",sum);
        first=second;
        second=sum;
        counter++;
    }
}