//Check whether given number is palindrome or not
#include <stdio.h>
#include <math.h>
void main()
{
    int n,reverse,orgnum,remainder;
    reverse=0;
    printf("Enter the number ");
    scanf("%d",&n);
    orgnum=n;
    while(n!=0)
    {
        remainder=n%10;
        reverse=reverse*10+remainder;
        n/=10;
    }
    (reverse==orgnum)?printf("Palindrome = %d\t%d\n",reverse,orgnum):printf("Not palindrome =  %d\t%d\n",reverse,orgnum);
}
