#include <stdio.h>
#include <string.h>
void myStrcat(char *a, char *b)
{
	int m = strlen(a);
	int n = strlen(b);
	int i;
	for (i = 0; i <= n; i++)
	a[m+i] = b[i];
}

int main()
{
	char str1[100] = "Geeks ";//it is stored in read /write memeory. this character array
	char *str2 = "Quiz";//It is stored in read only memory
	myStrcat(str1, str2);
	printf("%s ", str1);
	return 0;
}
//string and segmentation fault explained in this program