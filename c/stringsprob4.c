#include <stdio.h>

void my_toUpper(char* str, int index)
{
	*(str + index) &= ~32;
}

int main()
{
	char* arr = "geeksquiz";//char arr[5] = "geeksquiz"; - this code replacement prevent the segmentation fault 
	my_toUpper(arr, 0);
	my_toUpper(arr, 5);
	printf("%s", arr);
	return 0;
}
