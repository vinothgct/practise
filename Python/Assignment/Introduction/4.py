# Python Program to Calculate the Area of a Triangle
# method 1
base = float(input("enter the base of triangle "))
height = float(input("enter the height of triangle"))
area = 0.5*(base*height)
print("area of triangle is ",area)


# method 2

a = float(input('Enter first side: '))
b = float(input('Enter second side: '))
c = float(input('Enter third side: '))

# calculate the semi-perimeter
s = (a + b + c) / 2

# calculate the area
area = (s*(s-a)*(s-b)*(s-c)) ** 0.5
print('The area of the triangle is %0.2f' %area)
