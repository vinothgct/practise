# Python Program to Find the Square Root
# method 1
a = float(input("Enter the number to get square root "))
b = a ** 0.5
print("Square root of ",a ,"is ",b)


# method 2

import math as m
a = float(input("Enter the number to get square root "))
print("Square root of ",a ,"is ",m.sqrt(a))


#method 3
# it even work for complex numbers and negaive numbers

import cmath

# uncommment to take input from the user
num = float((input('Enter a number: ')))
num_sqrt = cmath.sqrt(num)
print('The square root of' ,num,' is ',num_sqrt)