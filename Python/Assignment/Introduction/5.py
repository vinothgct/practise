# Python Program to Solve Quadratic Equation
import cmath as cma
print("Enter value this format = ax^2+bx+c ")
a = float(input('enter a '))
b = float(input('enter b '))
c = float(input("enter c "))

d = cma.sqrt((b*b)-(4*a*c))

Quadratic_1 = ((-b)+(d))/(2*a)
Quadratic_2 = ((-b)-(d))/(2*a)
# Quadratic equation has two solution
print("solution 1: ",Quadratic_1,"\nsolution 2: ",Quadratic_2)