#IF and ELSE statement

number = int(input("enter number "))
if number%2==0:
    print("Given number is even")
else :
    print("Given number is odd")


#Nested IF ELSE 
mark1 = int(input("enter your SSLC mark out of 10"))
mark2 = int(input("enter your HSC mark out of 10"))
if mark1 > 6 and mark1<=10:
    if mark2>6 and mark2<=10:
        print("you are eligible to join engineering ")
    else:
        print("you are eligible to join Arts and Dipolamo courses")
else:
    print("your are not eliggible to continue your studies but again write the 10th exam and get good")


#OR Logical operator
fname = str(input("enter first name \n"))
lname = str(input("enter last name \n"))
if fname=="ganesh" or lname=="nt":
    print("he is a good guy")
else:
    print("he is not a good one")

#FOR LOOP Statement

for i in range(0,101,2):# this loop execute till reach 100 and it is incremented by 2 iteratively
    print(i)

for j in range(102,0,-2):
    print(j)

# FOR INTO FOR LOOP
for i in range(0,6):
    for a in range(3):
        print(a)




#WHILE LOOP statement
i=0
while i<10:
    print(i)
    i=i+1


# BREAK and PASS statement

for i in "ganeshan":
    if i=="h":
        continue 
    print(i)


for i in "ganeshan":
    if i=="h":
        break
    print(i)

for i in "ganeshan":
    if i=="h":
        pass 
    print(i)

